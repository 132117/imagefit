import pandas as pd
import ftplib
import argparse

parser = argparse.ArgumentParser(description='Fitment Update')

parser.add_argument('--username', help='FTP Username')
parser.add_argument('--password', help='FTP Password')

args=parser.parse_args()

ftp=ftplib.FTP('ftp.fitmentgroup.com',args.username,args.password)
ftp.retrbinary('RETR /FitmentData/tirewheel-smart-submodel-vehicles.csv',open('tirewheel-smart-submodel-vehicles.csv','wb').write)

data_tires=pd.read_csv('tirewheel-smart-submodel-vehicles.csv')

ftp=ftplib.FTP('ftp.fitmentgroup.com',args.username,args.password)
ftp.retrbinary('RETR /ImageData/VehicleImages.csv',open('VehicleImages.csv','wb').write)

data_images=pd.read_csv('VehicleImages.csv')

data_tires.loc[(data_tires['PMetric'].isnull())&(data_tires['LoadRange'].isnull()),'PMetric']='P'

print(data_images[["YearID","MakeName","ModelName"]].values)

data_images.index=pd.MultiIndex.from_tuples([tuple(x) for x in data_images[["YearID","MakeName","ModelName"]].values],names=["YearID","MakeName","ModelName"])
print(data_images)
print(data_tires[["Year","MakeName","ModelName"]])
data_tires=data_tires.join(data_images[['ImageURL_Small']],on=["Year","MakeName","ModelName"])
data_tires['VehimageURL']=data_tires['ImageURL_Small']
del data_tires['ImageURL_Small']

data_tires.to_csv('/home/public/vehicle_fitment/tirewheel-smart-submodel-vehicles-ss.csv',index=False)
